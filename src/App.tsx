import * as React from 'react';
import './App.css';
import JobForm from './components/JobForm';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import JobWrapper from './components/JobWrapper';
import JobListing from './components/JobListing';



export interface Props {

}

export interface State {


}


class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {

    };


  }

  render() {


    return (
      <div>

        <BrowserRouter>
          <Switch>

            <Route exact
              path={"/"}
              component={((url) => {
                return (
                  <JobWrapper ></JobWrapper>
                )
              })}
            />

            <Route exact
              path={"/JobForm"}
              component={((url) => {
                return (
                  <JobForm></JobForm>
                )
              })}
            />

            <Route exact
              path={"/JobListing"}
              component={((url) => {
                return (
                  <JobListing  ></JobListing>
                )
              })}
            />

          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
