export interface jobModel {
    jobId: number;
    jobTitle: string;
    jobLookingFor: string;
    jobExperience: string;
    jobEducation: string;
    jobSkills: string;
    jobDescription: string;
    jobImage: string;
    jobHourlyRate: number;
    jobExpectedStartDate: Date;
    jobCurrentLevel: string;
    Gender: string;
    jobEquipmentSpecification: string;
    jobWeekdays: weekDays;
}

export interface weekDays {
    monday: rangeDate;
    tuesday: rangeDate;
    wednesday: rangeDate;
    thursday: rangeDate;
    friday: rangeDate;
    saturday: rangeDate;
    sunday: rangeDate;
}

export interface rangeDate {
    istrue: boolean
    dateFrom: string,
    dateTo: string,
}