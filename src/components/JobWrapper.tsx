import axios from "axios";
import * as React from "react";
import { jobModel } from "../models/job-model";
import JobForm from "./JobForm";
import JobListing from "./JobListing";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export interface Props {

}

export interface State {
    showTab: string;
    jobListArray: jobModel[];
    jobObj: jobModel;
    count: number;
}

class JobWrapper extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            showTab: "Tab1",
            jobListArray: [],
            jobObj: Object(),
            count: 0,
        };

        this.onChangeJobHandler = this.onChangeJobHandler.bind(this);
        this.onChangeJobListHandler = this.onChangeJobListHandler.bind(this);
        this.onDeleteJobPostHandler = this.onDeleteJobPostHandler.bind(this);
    }

    async componentDidMount() {
        const url = " https://jsonplaceholder.typicode.com/users";
        await axios.get(`${url}`).then((resp) => { toast.info("Fetch Data via Axios Suceesfully", { autoClose: 2000, position: "bottom-left" }); console.log(resp.data) })

        let data = { name: "hamza", age: 23, email: "hamza@gmnail.com" }
        await axios.post(`${url}`, data).then((resp) => { toast.info("Post Data via Axios Suceesfully", { autoClose: 2000, position: "bottom-right" }); console.log(resp) }).catch((err) => {
            toast.info("Error Ocurred", { autoClose: 2000, position: "bottom-right" }); console.log(err)
        })



    }


    onChangeJobHandler(jobObjModel: jobModel) {

        let { jobListArray, count } = this.state;
        if (jobObjModel && jobObjModel.jobId) {

            let findIndex: jobModel = jobListArray && jobListArray.length > 0 && jobListArray.find(x => x.jobId == jobObjModel.jobId) || Object();
            jobListArray && jobListArray.length > 0 && jobListArray.splice(jobListArray.indexOf(findIndex), 1)
            jobListArray.push(jobObjModel);
            toast.success(`${jobObjModel && jobObjModel.jobTitle} Edited Succesfully`, { autoClose: 2000, position: "bottom-right" });
        }
        else {
            count = count + 1;
            jobObjModel.jobId = count;
            jobListArray.push(jobObjModel);
            toast.success("Job Post Save Succesfully", { autoClose: 2000, position: "bottom-right" });
        }
        this.setState({ jobListArray, count }, () => {
            this.setState({ showTab: "Tab1" })
        })
    }

    onChangeJobListHandler(job: jobModel) {
        let { jobListArray, jobObj } = this.state;
        jobObj = job;
        toast.success(`${jobObj.jobTitle} Edit`, { autoClose: 2000, position: "bottom-right" })
        this.setState({ jobObj, jobListArray }, () => {
            this.setState({ showTab: "Tab2" })
        })
    }

    onDeleteJobPostHandler(index: number) {
        let { jobListArray } = this.state;
        let findIndex: jobModel = jobListArray && jobListArray.length > 0 && jobListArray.find(x => x.jobId == index) || Object();
        toast.success(`Job Post Deleted Successfully`, { autoClose: 2000, position: "bottom-left" })
        jobListArray && jobListArray.length > 0 && jobListArray.splice(jobListArray.indexOf(findIndex), 1)
        this.setState({ jobListArray, jobObj: Object() })
    }



    render() {
        const { showTab, jobListArray, jobObj } = this.state;
        return (
            <div>

                {showTab === "Tab1" && <div className="content">
                    <div className="container">
                        <div className="container-fluid padding-10">
                            <div className="card padding-10">
                                <div className="row">
                                    <div className="col-md-8">
                                        <h3>Job Listing</h3>
                                    </div>
                                    <div className="col-md-4">
                                        <button className="btn btn-primary pull-right" onClick={() => { this.setState({ showTab: "Tab2", jobObj: Object() }) }}>Create New Job</button>
                                    </div>
                                    <p>Type something in the input field to search the table for job title:</p>
                                </div>
                                <JobListing jobListArrayProp={jobListArray} onChangeJobListHandler={this.onChangeJobListHandler} onDeleteJobPostHandler={this.onDeleteJobPostHandler} />
                            </div>
                        </div>
                    </div>
                </div>}

                {showTab === "Tab2" && <> <JobForm onChangeJobHandler={this.onChangeJobHandler} jobObjProps={jobObj} /> </>}
                <ToastContainer autoClose={1500} />

            </div>
        );
    }
}

export default JobWrapper;