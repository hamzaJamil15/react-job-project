import { data } from "jquery";
import * as React from "react";
import { jobModel } from "../models/job-model";


export interface Props {
    onChangeJobHandler?: (jobObj: jobModel) => void;
    jobObjProps?: jobModel;
}

export interface State {
    pageCount: number;
    jobObj: jobModel;
    jobListArray: jobModel[];
    isJobFormComplete: boolean;
    allWeekDays: weekDays

}

interface weekDays {
    monday: boolean;
    tuesday: boolean;
    wednesday: boolean;
    thursday: boolean;
    friday: boolean;
    saturday: boolean;
    sunday: boolean;
}

class JobForm extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            pageCount: 1,
            jobObj: Object(),
            jobListArray: [],
            isJobFormComplete: false,
            allWeekDays: Object(),
        }


        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.onChangeHandlerTime = this.onChangeHandlerTime.bind(this);
        this.checkValidate = this.checkValidate.bind(this);
        this.saveJobPost = this.saveJobPost.bind(this);
        this.fileUploader = this.fileUploader.bind(this);
    }

    componentDidMount() {
        let { jobObj } = this.state;

        if (this.props.jobObjProps && this.props.jobObjProps.jobTitle && this.props.jobObjProps.jobId) {
            jobObj = this.props.jobObjProps;
        }
        else {
            jobObj.jobWeekdays = Object();
            jobObj.jobWeekdays.monday = Object();
            jobObj.jobWeekdays.tuesday = Object();
            jobObj.jobWeekdays.wednesday = Object();
            jobObj.jobWeekdays.thursday = Object();
            jobObj.jobWeekdays.friday = Object();
            jobObj.jobWeekdays.saturday = Object();
            jobObj.jobWeekdays.sunday = Object();
            jobObj.jobWeekdays.monday.istrue = false;
            jobObj.jobWeekdays.tuesday.istrue = false;
            jobObj.jobWeekdays.wednesday.istrue = false;
            jobObj.jobWeekdays.thursday.istrue = false;
            jobObj.jobWeekdays.friday.istrue = false;
            jobObj.jobWeekdays.saturday.istrue = false;
            jobObj.jobWeekdays.sunday.istrue = false;
        }
        this.setState({ jobObj }, async () => { await this.checkValidate() })
    }

    onChangeHandler(attrName: string, val: string, valDATE: Date) {
        let { jobObj } = this.state;
        switch (attrName) {
            case "jobTitle":
                jobObj.jobTitle = val;
                break;
            case "jobExperience":
                jobObj.jobExperience = val;
                break;
            case "jobEducation":
                jobObj.jobEducation = val;
                break;
            case "jobDescription":
                jobObj.jobDescription = val;
                break;
            case "jobHourlyRate":
                jobObj.jobHourlyRate = parseInt(val);
                break;
            case "jobExpectedStartDate":
                jobObj.jobExpectedStartDate = valDATE;
                break;
            case "jobCurrentLevel":
                jobObj.jobCurrentLevel = val;
                break;
            case "jobEquipmentSpecification":
                jobObj.jobEquipmentSpecification = val;
                break;
            case "jobSkills":
                jobObj.jobSkills = val;
                break;
            case "Gender":
                jobObj.Gender = val;
                break;
            case "jobTitle":
                jobObj.jobTitle = val;
                break;
            case "jobTitle":
                jobObj.jobTitle = val;
                break;
            case "jobTitle":
                jobObj.jobTitle = val;
                break;
            case "jobTitle":
                jobObj.jobTitle = val;
                break;
            case "jobImage":
                console.log();
                break;

        }
        this.setState({ jobObj }, async () => {
            await this.checkValidate();
        });
    }

    async onChangeHandlerTime(attrName: string, val) {
        let { jobObj } = this.state;
        switch (attrName) {
            case "mondayTo":
                jobObj.jobWeekdays.monday.dateTo = val;
                break;
            case "mondayFrom":
                jobObj.jobWeekdays.monday.dateFrom = val;
                break;
            case "tuesdayTo":
                jobObj.jobWeekdays.tuesday.dateTo = val;
                break;
            case "tuesdayFrom":
                jobObj.jobWeekdays.tuesday.dateFrom = val;
                break;
            case "wednesdayTo":
                jobObj.jobWeekdays.wednesday.dateTo = val;
                break;
            case "wednesdayFrom":
                jobObj.jobWeekdays.wednesday.dateFrom = val;
                break;
            case "thursdayTo":
                jobObj.jobWeekdays.thursday.dateTo = val;
                break;
            case "thursdayFrom":
                jobObj.jobWeekdays.thursday.dateFrom = val;
                break;
            case "fridayTo":
                jobObj.jobWeekdays.friday.dateTo = val;
                break;
            case "fridayFrom":
                jobObj.jobWeekdays.friday.dateFrom = val;
                break;
            case "saturdayTo":
                jobObj.jobWeekdays.saturday.dateTo = val;
                break;
            case "saturdayFrom":
                jobObj.jobWeekdays.saturday.dateFrom = val;
                break;
            case "sundayTo":
                jobObj.jobWeekdays.sunday.dateTo = val;
                break;
            case "sundayFrom":
                jobObj.jobWeekdays.sunday.dateFrom = val;
                break;

        }
        this.setState({ jobObj })
        await this.checkValidate()
    }

    checkValidate() {
        let { isJobFormComplete, jobObj } = this.state
        debugger;
        if (jobObj.jobTitle && jobObj.jobExperience && jobObj.jobImage && (jobObj.jobHourlyRate && jobObj.jobHourlyRate > 10)) {

            isJobFormComplete = true;
            let count: number = 0;
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.monday && jobObj.jobWeekdays.monday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.tuesday && jobObj.jobWeekdays.tuesday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj.jobWeekdays.wednesday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.thursday && jobObj.jobWeekdays.thursday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.friday && jobObj.jobWeekdays.friday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.saturday && jobObj.jobWeekdays.saturday.istrue) { count++; }
            if (jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.sunday && jobObj.jobWeekdays.sunday.istrue) { count++; }

            if (count >= 2) {
                console.log("Atleast 2 days selected")
                isJobFormComplete = true;
                if ((jobObj.jobWeekdays.monday.istrue && jobObj.jobWeekdays.monday.dateTo && jobObj.jobWeekdays.monday.dateFrom) || (jobObj.jobWeekdays.tuesday.istrue && jobObj.jobWeekdays.tuesday.dateTo && jobObj.jobWeekdays.tuesday.dateFrom)
                    || (jobObj.jobWeekdays.wednesday.istrue && jobObj.jobWeekdays.wednesday.dateTo && jobObj.jobWeekdays.wednesday.dateFrom) || (jobObj.jobWeekdays.thursday.istrue && jobObj.jobWeekdays.thursday.dateTo && jobObj.jobWeekdays.thursday.dateFrom)
                    || (jobObj.jobWeekdays.friday.istrue && jobObj.jobWeekdays.friday.dateTo && jobObj.jobWeekdays.friday.dateFrom) || (jobObj.jobWeekdays.saturday.istrue && jobObj.jobWeekdays.saturday.dateTo && jobObj.jobWeekdays.saturday.dateFrom)
                    || (jobObj.jobWeekdays.sunday.istrue && jobObj.jobWeekdays.sunday.dateTo && jobObj.jobWeekdays.sunday.dateFrom)) {


                    if (jobObj.jobWeekdays.monday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.monday.dateTo && jobObj.jobWeekdays.monday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.monday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.monday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];
                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }

                    }
                    if (jobObj.jobWeekdays.tuesday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.tuesday.dateTo && jobObj.jobWeekdays.tuesday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.tuesday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.tuesday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                    if (jobObj.jobWeekdays.wednesday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.wednesday.dateTo && jobObj.jobWeekdays.wednesday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.wednesday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.wednesday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                    if (jobObj.jobWeekdays.thursday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.thursday.dateTo && jobObj.jobWeekdays.thursday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.thursday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.thursday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                    if (jobObj.jobWeekdays.friday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.friday.dateTo && jobObj.jobWeekdays.friday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.friday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.friday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                    if (jobObj.jobWeekdays.saturday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.saturday.dateTo && jobObj.jobWeekdays.saturday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.saturday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.saturday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                    if (jobObj.jobWeekdays.sunday.istrue) {
                        if (isJobFormComplete) {
                            if (jobObj.jobWeekdays.sunday.dateTo && jobObj.jobWeekdays.sunday.dateFrom) {
                                let a: string[] = jobObj.jobWeekdays.sunday.dateTo.split(":");
                                let b: string[] = jobObj.jobWeekdays.sunday.dateFrom.split(":");
                                let c = a[0] + a[1];
                                let d = b[0] + b[1];

                                if (parseInt(c) == parseInt(d)) {
                                    console.log("Yes Equal")
                                    isJobFormComplete = false
                                }
                                else {
                                    let result: number = parseInt(c) - parseInt(d)
                                    result = result > 0 ? result : result * -1
                                    if (result >= 800) { isJobFormComplete = true; console.log("more than 9 hours") }
                                    else { isJobFormComplete = false }
                                }
                            }
                            else { isJobFormComplete = false }

                        }
                    }
                }
                else {

                    isJobFormComplete = false
                }


            }
            else { isJobFormComplete = false }

        }
        else {
            isJobFormComplete = false;
        }
        this.setState({ isJobFormComplete })
    }


    saveJobPost() {
        const { jobObj } = this.state;
        if (this.props.onChangeJobHandler)
            this.props.onChangeJobHandler(jobObj);
    }


    fileUploader(event) {
        let { jobObj } = this.state;
        let file = event.target.files[0];
        if (file) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function () {
                let base64: any = reader.result;
                jobObj.jobImage = base64;
            };
        }
        else{
            jobObj.jobImage = "";
        }
        this.setState({ jobObj }, async () => { await this.checkValidate() })
    }

    render() {
        const { pageCount, jobObj, isJobFormComplete } = this.state;
        const alertDisplay = { boxShadow: "1px 1px 5px red" };
        const normalDisplay = { boxShadow: "none" };
        return (
            <div>
                <div className="container padding-10">

                    <div className="row">
                        <div className="col-md-12">
                            <h2>Create a Job Post</h2>
                            <p>Complete the following steps to create an efective job post:</p>
                            <h4>Step {pageCount}/3</h4>
                        </div>
                    </div>

                    {/* Tabs */}
                    <ul className="nav justify-content-center">
                        <li className="nav-item" style={{ backgroundColor: `${pageCount == 1 ? "cadetblue" : ""}`, borderTopRightRadius: "15px", borderBottomRightRadius: "15px" }}>
                            <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} aria-current="page" onClick={() => { this.setState({ pageCount: 1 }) }} >Job Information</a>
                        </li>
                        <li className="nav-item" style={{ backgroundColor: `${pageCount == 2 ? "cadetblue" : ""}`, borderTopRightRadius: "15px", borderBottomRightRadius: "15px" }}>
                            <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => { this.setState({ pageCount: 2 }) }} >Candidate Type</a>
                        </li>
                        <li className="nav-item" style={{ backgroundColor: `${pageCount == 3 ? "cadetblue" : ""}`, borderTopRightRadius: "15px", borderBottomRightRadius: "15px" }}>
                            <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => { this.setState({ pageCount: 3 }) }} >Shift Timing</a>
                        </li>
                    </ul>
                    <br />


                    {/* pages */}

                    <div className="container-fluid">
                        <div className="card">

                            {/* page1 */}
                            {pageCount == 1 &&
                                <div className="row" style={{ padding: "20px" }}>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="text" placeholder="LookingFor" value={jobObj.jobTitle} className="form-control" onChange={(event) => { this.onChangeHandler("jobTitle", event.target.value, new Date) }}
                                            style={jobObj.jobTitle ? normalDisplay : alertDisplay} />
                                    </div>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="text" placeholder="Experience" value={jobObj.jobExperience} className="form-control"
                                            style={jobObj.jobExperience ? normalDisplay : alertDisplay} onChange={(event) => { this.onChangeHandler("jobExperience", event.target.value, new Date) }} />
                                    </div>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="text" placeholder="Education" value={jobObj.jobEducation} className="form-control"
                                            onChange={(event) => { this.onChangeHandler("jobEducation", event.target.value, new Date) }} />
                                    </div>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="file" placeholder="Education"   className="form-control" style={jobObj.jobImage ? normalDisplay : alertDisplay}
                                            onChange={(e) => { this.fileUploader(e) }} />
                                    </div>
                                    <div className="col-md-12" style={{ padding: "5px" }}>
                                        <input type="text" placeholder="Skills" value={jobObj.jobSkills} className="form-control"
                                            onChange={(event) => { this.onChangeHandler("jobSkills", event.target.value, new Date) }} />
                                    </div>
                                    <div className="col-md-12" style={{ padding: "5px" }}>
                                        <textarea name="" id="" cols={30} rows={6} placeholder="Description" value={jobObj.jobDescription} className="form-control"
                                            onChange={(event) => { this.onChangeHandler("jobDescription", event.target.value, new Date) }} ></textarea>
                                    </div>
                                </div>}

                            {/* page 2 */}
                            {pageCount == 2 &&
                                <div className="row" style={{ padding: "20px" }}>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="number" placeholder="HourlyRate" value={jobObj.jobHourlyRate} className="form-control" style={jobObj.jobHourlyRate ? normalDisplay : alertDisplay}
                                            onChange={(event) => { this.onChangeHandler("jobHourlyRate", event.target.value, new Date) }} />
                                    </div>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="date" placeholder="Expected Start Date" value={new Date(jobObj.jobExpectedStartDate).toLocaleString()} className="form-control"
                                            onChange={(event) => { this.onChangeHandler("jobExpectedStartDate", "", new Date(event.target.value)) }} />
                                    </div>
                                    <div className="col-md-6" style={{ padding: "5px" }}>
                                        <input type="text" placeholder="Current Level" value={jobObj.jobCurrentLevel} onChange={(event) => { this.onChangeHandler("jobCurrentLevel", event.target.value, new Date) }} className="form-control" />
                                    </div>
                                    <div className="col-md-12" style={{ padding: "5px" }}>
                                        <select name="Gender" id="" className="form-control" value={jobObj.Gender} onChange={(event) => { this.onChangeHandler("Gender", event.target.value, new Date) }}>
                                            <option value="">Select Gender</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                    </div>
                                    <div className="col-md-12" style={{ padding: "5px" }}>
                                        {/* <input type="text" placeholder="Description" className="form-control" /> */}
                                        <textarea name="" id="" cols={30} rows={6} placeholder="Equipment Specification" value={jobObj.jobEquipmentSpecification} className="form-control"
                                            onChange={(event) => { this.onChangeHandler("jobEquipmentSpecification", event.target.value, new Date) }} ></textarea>
                                    </div>
                                </div>}

                            {/* page 3 */}

                            {pageCount == 3 && <>

                                {/* // week days tab */}
                                <ul className="nav justify-content-center">
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.monday && jobObj.jobWeekdays.monday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} aria-current="page"
                                            onClick={() => {
                                                let { jobObj } = this.state;
                                                if (jobObj && jobObj.jobWeekdays)
                                                    jobObj.jobWeekdays.monday.istrue = !jobObj.jobWeekdays.monday.istrue;
                                                this.setState({ jobObj }, async () => { await this.checkValidate() })
                                            }} >Mon</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.tuesday && jobObj.jobWeekdays.tuesday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.tuesday.istrue = !jobObj.jobWeekdays.tuesday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Tues</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj.jobWeekdays.wednesday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.wednesday.istrue = !jobObj.jobWeekdays.wednesday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Wed</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.thursday && jobObj.jobWeekdays.thursday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.thursday.istrue = !jobObj.jobWeekdays.thursday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Thurs</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.friday && jobObj.jobWeekdays.friday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.friday.istrue = !jobObj.jobWeekdays.friday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Fri</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.saturday && jobObj.jobWeekdays.saturday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.saturday.istrue = !jobObj.jobWeekdays.saturday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Sat</a>
                                    </li>
                                    <li className="nav-item" style={{ backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.sunday && jobObj.jobWeekdays.sunday.istrue ? "cadetblue" : ""}`, borderRadius: "15px" }}>
                                        <a className="nav-link" style={{ color: "black", fontSize: "18px", cursor: "pointer" }} onClick={() => {
                                            let { jobObj } = this.state;
                                            if (jobObj && jobObj.jobWeekdays)
                                                jobObj.jobWeekdays.sunday.istrue = !jobObj.jobWeekdays.sunday.istrue;
                                            this.setState({ jobObj }, async () => { await this.checkValidate() })
                                        }} >Sun</a>
                                    </li>
                                </ul>

                                <div className="row" style={{ padding: "10px" }}>

                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.monday && jobObj.jobWeekdays.monday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Monday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.monday && jobObj.jobWeekdays.monday.dateTo} onChange={(event) => { this.onChangeHandlerTime("mondayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.monday && jobObj.jobWeekdays.monday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("mondayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.tuesday && jobObj.jobWeekdays.tuesday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Tuesday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.tuesday && jobObj.jobWeekdays.tuesday.dateTo} onChange={(event) => { this.onChangeHandlerTime("tuesdayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.tuesday && jobObj.jobWeekdays.tuesday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("tuesdayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <br /><br /><br />

                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj.jobWeekdays.wednesday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Wednesday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj.jobWeekdays.wednesday.dateTo} onChange={(event) => { this.onChangeHandlerTime("wednesdayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj.jobWeekdays.wednesday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("wednesdayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.wednesday && jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.thursday && jobObj.jobWeekdays.thursday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Thursday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.thursday && jobObj.jobWeekdays.thursday.dateTo} onChange={(event) => { this.onChangeHandlerTime("thursdayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.thursday && jobObj.jobWeekdays.thursday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("thursdayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.friday && jobObj.jobWeekdays.friday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Friday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.friday && jobObj.jobWeekdays.friday.dateTo} onChange={(event) => { this.onChangeHandlerTime("fridayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.friday && jobObj.jobWeekdays.friday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("fridayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.saturday && jobObj.jobWeekdays.saturday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Saturday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.saturday && jobObj.jobWeekdays.saturday.dateTo} onChange={(event) => { this.onChangeHandlerTime("saturdayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.saturday && jobObj.jobWeekdays.saturday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("saturdayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-12">
                                        <div className="padding-10" style={{ border: "1px solid black", backgroundColor: `${jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.sunday && jobObj.jobWeekdays.sunday.istrue ? "cadetblue" : ""}` }}>
                                            <h4 style={{ textAlign: "center" }}>Sunday</h4>
                                            <div className="row">
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>To</h5>
                                                    </label>
                                                    <input type="time" name="to" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.sunday && jobObj.jobWeekdays.sunday.dateTo} onChange={(event) => { this.onChangeHandlerTime("sundayTo", event.target.value) }} />
                                                </div>
                                                <div className="col-md-6">
                                                    <label htmlFor="">
                                                        <h5>From</h5>
                                                    </label>
                                                    <input type="time" name="from" id="" className="form-control" value={jobObj && jobObj.jobWeekdays && jobObj.jobWeekdays.sunday && jobObj.jobWeekdays.sunday.dateFrom} onChange={(event) => { this.onChangeHandlerTime("sundayFrom", event.target.value) }} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                </div>

                            </>}

                            {/* next | prev button */}
                            <div className="row" style={{ padding: "10px" }}>
                                <div className="col-md-16">
                                    <button className="btn btn-primary" disabled={pageCount === 1 ? true : false} onClick={() => {
                                        let { pageCount } = this.state;
                                        pageCount--;
                                        this.setState({ pageCount })
                                    }}>Previous</button>
                                    <button className="btn btn-primary" style={{ float: "right" }} disabled={pageCount === 3 ? true : false} onClick={() => {
                                        let { pageCount } = this.state;
                                        pageCount++;
                                        this.setState({ pageCount })
                                    }}>Next</button>
                                </div>
                            </div>




                            {/* save | cancel button */}
                            <div className="row padding-10">
                                <div className="col-md-12">
                                    <button className="btn btn-primary" disabled={!isJobFormComplete} onClick={this.saveJobPost}>Save</button>
                                    <button className="btn btn-primary" style={{ marginLeft: "5px" }} onClick={() => {
                                        let job: jobModel = Object();
                                        if (this.props.onChangeJobHandler)
                                            this.props.onChangeJobHandler(job)
                                    }}>Cancel</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div >
        );
    }
}

export default JobForm;