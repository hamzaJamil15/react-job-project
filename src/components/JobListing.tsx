import * as React from "react";
import { jobModel } from "../models/job-model";


export interface Props {
    jobListArrayProp?: jobModel[];
    onChangeJobListHandler?: (jobObj: jobModel) => void;
    onDeleteJobPostHandler?: (index: number) => void;
}

export interface State {
    jobListArray: jobModel[];
    jobTitleSearch: string;
}

class JobListing extends React.Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            jobListArray: [],
            jobTitleSearch: "",
        }
    }

    componentDidMount() {
        let { jobListArray } = this.state;
        let { jobListArrayProp } = this.props;
        
        jobListArrayProp && jobListArrayProp.length > 0 && jobListArrayProp[0] && jobListArrayProp.map((x) => {
            // x.jobImage = "https://sharp-pakistan.org/wp-content/uploads/2017/09/jobs.jpg";
            jobListArray.push(x)
        })
       
        this.setState({ jobListArray })
    }


    componentWillReceiveProps(props: Props) {
        let { jobListArray } = this.state;
        jobListArray = []
        props.jobListArrayProp && props.jobListArrayProp.length > 0 && props.jobListArrayProp[0] && props.jobListArrayProp.map((x) => {
            // x.jobImage = "https://sharp-pakistan.org/wp-content/uploads/2017/09/jobs.jpg";
            jobListArray.push(x)
        })
        this.setState({ jobListArray })
    }


    render() {
        const { jobListArray, jobTitleSearch } = this.state;
        let filteredJob: jobModel[] = jobTitleSearch ? jobListArray.filter(x => x.jobTitle.toLowerCase().includes(jobTitleSearch.toLowerCase().trim())) : jobListArray;
        return (
            <div>
                <div className="container padding-10">
                    <div className="row">
                        <input type="text" className="form-control" placeholder="Search" onChange={(event) => { this.setState({ jobTitleSearch: event.target.value }) }} />
                    </div>
                    <br />
                    <div style={{ height: "300px", overflowX: "scroll" }}>
                        <table className="table table-bordered" >
                            <thead>
                                <tr>
                                    <th>Job Title</th>
                                    <th>Experience</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                {filteredJob && filteredJob.length > 0 && filteredJob.map((od, oi) => {
                                    return <>
                                        <tr>
                                            <td>{od.jobTitle}</td>
                                            <td>{od.jobExperience}</td>
                                            <td><img src={(od.jobImage).toString()} alt="" width="150px" height="50px" /></td>
                                            <td style={{ textAlign: "center" }}>
                                                <button className="btn btn-primary" onClick={() => {
                                                    if (this.props.onChangeJobListHandler)
                                                        this.props.onChangeJobListHandler(od)
                                                }} >
                                                    <i className="fa fa-edit"></i>
                                                </button>
                                                <button className="btn btn-danger" style={{ marginLeft: "10px" }} onClick={() => {
                                                    if (this.props.onDeleteJobPostHandler)
                                                        this.props.onDeleteJobPostHandler(oi)
                                                }}>
                                                    <i className="fa fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default JobListing;